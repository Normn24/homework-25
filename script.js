"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    this._name = newName;
  }

  get age() {
    return this._age;
  }

  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }

  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  get languages() {
    return this.lang;
  }

  set languages(newLang) {
    this.lang = newLang;
  }
}

const programmer1 = new Programmer("Jake", 19, 1000, ["JavaScript", "Python"]);
const programmer2 = new Programmer("Adam", 20, 3000, ["C++", "Go", "JavaScript", "Python"]);

console.log(programmer1);
console.log(programmer2);
